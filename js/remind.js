/* Site Reminder Chrome Extension
 * Author : Amanjeev Sethi
 * July 2012
 * Background process
 */

/* 
 * Globals - Mostly HTML element references
 */

var now = (new Date()).getTime()/1000;
var timeToRemindInElement = $("#timein");
var notesElement = $("#notes");
var reminderdiff = $("#reminderdiff");
var removeresetElem = $("#removereset");
var timeTypeElement = $("#timetype");
var saveLink = $("#save");
var messageElement = $("#message");
var resultTimePane = $("#time");
var reminderinput = $("#reminderinput");
var refreshElement = $("#refresh");
var reminderlistElement = $("#reminderlist");
var defaultSnooze = 10 * 60; //10 minutes

/* 
 * updateUrlTimes
 * Inputs : 
 *  now = the current system time 
 *  timeToRemindIn = the current time added with the number if minutes/hours/days set by the user
 *  notes = Extra notes field that the user wants to save
 *  
 */

function updateUrlTimes(now, timeToRemindIn, notes) {
  //Hide the elements in the beginning and later show as needed
  $(removeresetElem).hide();
  $(reminderdiff).empty();
  $(messageElement).empty();

  chrome.tabs.getSelected(null, function(tab) {
    var currentUrl = tab.url;

    //Case 1 = if the current url is not present in the localStorage
    if(!localStorage[currentUrl]) {
      if (timeToRemindIn != null) {
        var jsonObject = {timeNow : now, timeToRemindIn: now + timeToRemindIn, notes: notes};
        localStorage[currentUrl] = JSON.stringify(jsonObject);
      } 
      $(reminderinput).show();


    // Case 2 = if our current url is already in the localStorage
    // i.e. the user has already visited and set a reminder for this page 
    // Reminder could be stale
    } else if (localStorage[currentUrl]) {
      var jsonRetObject = JSON.parse(localStorage[currentUrl]);
      var differenceUpdate = parseInt(jsonRetObject.timeToRemindIn) - parseInt(now);
      var originalNotes = jsonRetObject.notes;

      /*
      console.log("here after differenceUpdate = ");
      console.log(differenceUpdate);
      console.log(jsonRetObject.timeToRemindIn);
      console.log(now);
      */
      //If the reminder is stale
      if (differenceUpdate > 0 ) {
        $(reminderinput).hide(); //hide the input because time already set
        $(removeresetElem).show();
        //$(reminderdiff).empty().html("Reminder for this site set in " + differenceUpdate.toString());
        $(reminderdiff).empty().html("A reminder for this site is set");
      } else {
        $(removeresetElem).show();
        $(notesElement).val(jsonRetObject.notes);
      }

      if (parseInt(jsonRetObject.timeNow) < now) {
        if (timeToRemindIn != null) {
          var jsonObject = {timeNow : now, timeToRemindIn: now + timeToRemindIn, notes: $(notesElement).val()};
          localStorage[currentUrl] = JSON.stringify(jsonObject);
        }  
        
      }
    }

    if (timeToRemindIn != null) {
      $(messageElement).html("<p>Reminder set in " + timeToRemindIn + "</p>");
      $(removeresetElem).show();
      refresh();
    }

  });
}

function refresh() {
  location.reload(true);
}


function deleteSiteReminder() {
  chrome.tabs.getSelected(null, function(tab) {
    var currentUrl = tab.url;
    if (localStorage[currentUrl]) {
      localStorage.removeItem(currentUrl);  
    }
  });
  refresh();
}


$("#reset").click(function() {
  $(reminderinput).show();
  chrome.tabs.getSelected(null, function(tab) {
    var currentUrl = tab.url;
    if (localStorage[currentUrl]) {
      var jsonRetObject = JSON.parse(localStorage[currentUrl]);
      $(notesElement).val(jsonRetObject.notes);
    }
  });
});


$("#remove").click(function() {
  deleteSiteReminder();
  var now = (new Date()).getTime()/1000;
  updateUrlTimes(now, null, "");
});


$(refreshElement).click(function() {
  refresh();
});

$(saveLink).click(function() {
  var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
  var now = parseInt((new Date()).getTime()/1000);
  var timeToRemindIn = $(timeToRemindInElement).val();
  var timeType = $(timeTypeElement).val();
  var notes = $(notesElement).val();

  if(numberRegex.test(timeToRemindIn)) {
    //console.log('I am a number');
    if (timeType === "hours") {
      timeToRemindIn = timeToRemindIn * 60 * 60;
    } else if (timeType === "minutes") {
      timeToRemindIn = timeToRemindIn * 60;
    } else if (timeType === "days") {
      timeToRemindIn = timeToRemindIn * 60 * 60 * 24;
    }
    updateUrlTimes(now, timeToRemindIn, notes);
  } else {
    alert("Please enter a number");
  }
  /*
  console.log("type = " + timeType);
  console.log("now = " + now);
  console.log("tottal time to remind  = ");
  console.log( parseFloat(timeToRemindIn) + parseFloat(now));
  */
});

$(function() {
  //First call. Passing null for timeToRemindIn and Empty string for the notes
  updateUrlTimes(now, null, "");
  $(reminderlistElement).empty();
  if (localStorage.length != 0) {

    for (var i = 0; i < localStorage.length; i++) {
      var jsonRetObject = JSON.parse(localStorage[localStorage.key(i)]);
      var listElem = $('<li>');
      var checked = false;
      var now = parseInt((new Date()).getTime()/1000);
      if (now >= parseInt(jsonRetObject.timeToRemindIn)) {
        $(listElem).css('color', '#FF0000');
        checked = true;
      }
      $(listElem).addClass('reminderlistelement').append(
        $('<p>').append(
            $('<span>').append(
              $('<input>').attr({
                'type': 'checkbox',
                'value' : localStorage.key(i),
                'checked' : checked,
                'class' : 'remindercheckbox remindercheckbox' + i.toString(),
                'name' : 'remindercheckbox' + i.toString(),
                'id' : 'remindercheckbox' + i.toString()
              })
            )
          ).append(
            $('<label>').attr({
              'for' : 'remindercheckbox' + i
            }).append(
              localStorage.key(i)
            )
          ).append(
            $('<p>').attr({
              'class' : 'noteslist'
            }).append(
              jsonRetObject.notes
            )
          )
        );

      $(reminderlistElement).append(listElem);

      $('.linkopen').click(function() {
        //console.log($(this).prop('href'));
        chrome.tabs.create({
          'url' : $(this).attr('href')
        });
      });

      $('.remindercheckbox').change(function() {
        //console.log("sss");
        if ($(this).is(':checked')) {
          $(this).parent().parent().parent().addClass('highlightbg');
        } else {
          $(this).parent().parent().parent().removeClass('highlightbg');
        }
      });


    }

    $("#openInList").click(function() {
      var checkboxes = $('.reminderlistelement input[type=checkbox]');
      if (checkboxes && checkboxes.length > 0) {
        for(var i = 0; i < checkboxes.length; i++) {
          if ($(checkboxes[i]).is(':checked')) {
            chrome.tabs.create({
              'url' : $(checkboxes[i]).val()
            });
          }
        }  
      }
    });

    $("#removeInList").click(function() {
      var checkboxes = $('.reminderlistelement input[type=checkbox]');
      if (checkboxes && checkboxes.length > 0) {
        for(var i = 0; i < checkboxes.length; i++) {
          if ($(checkboxes[i]).is(':checked')) {
            localStorage.removeItem($(checkboxes[i]).val()); 
          }
        }  
      }
      refresh();
    });

    $("#snoozeInList").click(function() {
      var checkboxes = $('.reminderlistelement input[type=checkbox]');
      if (checkboxes && checkboxes.length > 0) {
        for(var i = 0; i < checkboxes.length; i++) {
          if ($(checkboxes[i]).is(':checked')) {
            var link = $(checkboxes[i]).val(); 
            var jsonRetObject = JSON.parse(localStorage[link]);
            var now = parseInt((new Date()).getTime()/1000);
            var jsonObject = {timeNow : now, timeToRemindIn: now + defaultSnooze, notes: jsonRetObject.notes};
            localStorage.removeItem(link);
            localStorage[link] = JSON.stringify(jsonObject);
          }
        }  
      }
      refresh();
    });
  }
});


$("#checkall").click(function() {
  if ($(this).is(':checked')) {
    $('.remindercheckbox').each(function() {
      $(this).prop('checked', true);
      $(this).parent().parent().parent().addClass('highlightbg');
    });
  } else {
    $('.remindercheckbox').each(function() {
      $(this).prop('checked', false);
      $(this).parent().parent().parent().removeClass('highlightbg');
    });
  }
});

$("#shownotes").change(function() {
  if($(this).is(':checked')) {
    $("#notes").show();
  } else {
    $("#notes").hide();
  }
});














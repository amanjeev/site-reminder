/* Site Reminder Chrome Extension
 * Author : Amanjeev Sethi
 * July 2012
 * Background process
 */

var counter = 0;
var timerId;
var now = parseInt((new Date()).getTime()/1000);


function listenAndRemind() {
	var reminders = 0;
	counter++;

	if (localStorage.length != 0) {
		for (var i = 0; i < localStorage.length; i++) {
			var nowT = parseInt((new Date()).getTime()/1000);
			var currentUrl = localStorage.key(i);
			var jsonRetObject = JSON.parse(localStorage[currentUrl]);
			if (parseInt(jsonRetObject.timeToRemindIn) <= nowT){
				reminders++;
			}
		}
		if (reminders > 0) {
			chrome.browserAction.setIcon({path: 'img/reminder.png'});
			chrome.browserAction.setBadgeText({text: reminders.toString()});
			chrome.browserAction.setTitle({'title' : 'Site Reminder - ' + reminders.toString() + ' reminder(s)' })
		} else {
			chrome.browserAction.setIcon({path: 'img/icon.png'});
			chrome.browserAction.setBadgeText({text: ""});	
		}
		
	} else {
		chrome.browserAction.setIcon({path: 'img/icon.png'});
		chrome.browserAction.setBadgeText({text: ""});
	}
}

setInterval(listenAndRemind, 1000);
